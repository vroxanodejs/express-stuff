const express = require("express");
const router = express.Router();
const Post = require("../models/Post");

// Routes
// We are on he root of /posts
router.get("/", async (req, res) => {
	try {
		const posts = await Post.find();
		res.json(posts);
	} catch (err) {
		res.json({ message: err });
	}
});

// Retrieve an specific post
router.get("/:postId", async (req, res) => {
	try {
		const post = await Post.findById(req.params.postId);
		res.json(post);
	} catch (err) {
		res.json({ message: err });
	}
});

// Deleting a single post
router.delete("/:postId", async (req, res) => {
	try {
		const post = await Post.remove({ _id: req.params.postId });
		res.json({ message: "Deleted one", object: post });
	} catch (err) {
		res.json({ message: err });
	}
});

// Updating a post
router.patch("/:postId", async (req, res) => {
	try {
		const post = await Post.updateOne(
			{ _id: req.params.postId },
			{
				$set: {
					title: req.body.title
				}
			}
		);
		res.json({ message: "Updated one", object: post });
	} catch (err) {
		res.json({ message: err });
	}
});

// Submitting a post
router.post("/", async (req, res) => {
	const post = new Post({
		title: req.body.title,
		description: req.body.description,
		author: req.body.author
	});
	try {
		const savedPost = await post.save();
		res.json(savedPost);
	} catch (err) {
		res.json({ message: err });
	}
});

module.exports = router;

const express = require("express");
const mongoose = require("mongoose"); // MongoDB features
const bodyParser = require("body-parser"); // Parse the body of a HTTP Request
const cors = require("cors"); // Allow Cross Origin Access
require("dotenv/config");

const app = express();
app.use(cors());
app.use(bodyParser.json());

// Import routes
const postsRoutes = require("./routes/posts");
app.use("/posts", postsRoutes);

// Middlewares
app.use("/posts", () => {
	// Triggered whenever that route gets that route
	console.log("We are on a middleware");
});

// DB connection (MongoDB Atlas)
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }, () => {
	console.log("Connected to DB");
});

// Start listening
app.listen(8090);
